# Dup-DB-Builder

Simple, q&d bash script that produces ANSI-SQL-1992-compliant insert statements
about files in a given path, storing the following information:

- Storage device identifier (to be supplied by the user)
- Checksum (MD5)
- Size
- Filename
- Full Path

Aims to be useful for finding duplicates on separate devices that are not online
(i.e. not connected to the same computer/network at the same time).

# Quickstart (using MariaDB)

First, create a MariaDB database for using dupdbbuilder.
(assumes you have mariadb-server already installed)

Connect to MariaDB:

    mysql -uroot -p
    
Create user and new database (as root):

    CREATE USER 'dupsdbuser'@'localhost' IDENTIFIED BY 'password';
    GRANT ALL PRIVILEGES ON dupsdb . * TO 'dupsdbuser'@'localhost';
    CREATE DATABASE dupsdb CHARACTER SET utf8mb4;

Populate the schema:

    mysql -udupsdbuser -ppassword dupsdb < schema.sql
    
Now, run dupsdbbuilder on some directory and check if it generates plausible SQL statements:

    ./dupdbbuilder.sh /usr/lib current-usrlib | tee inserts.sql
    
It should print out something like this to the console and to `inserts.sql`:

    INSERT INTO DUPS (DEV_ID,MD5,SIZE,NAME,PATH) VALUES ('current-lib-dir','552c49d6bc04ca710579b6784bceefb0',18,'libk3blib.so.6','/usr/lib/libk3blib.so.6.0.0');
    INSERT INTO DUPS (DEV_ID,MD5,SIZE,NAME,PATH) VALUES ('current-lib-dir','cfd16a23fc982d1604a6c68b47f02906',894000,'libkjs.so.4.14.26','/usr/lib/libkjs.so.4.14.26');
    INSERT INTO DUPS (DEV_ID,MD5,SIZE,NAME,PATH) VALUES ('current-lib-dir','7a2a0c73e10b73ac0023106e95001ee4',376064,'libkopete_oscar.so.4.14.26','/usr/lib/libkopete_oscar.so.4.14.26');
    INSERT INTO DUPS (DEV_ID,MD5,SIZE,NAME,PATH) VALUES ('current-lib-dir','e41f3676882fa3cd73e9cb427c7998bd',86456,'libbtparse.so.1','/usr/lib/libbtparse.so.1');
    
If this looks okay, you can use `inserts.sql` to populate your database:


    mysql -udupsdbuser -ppassword dupsdb < inserts.sql
        
That's it!
