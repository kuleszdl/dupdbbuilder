#!/bin/bash
#    This file is part of Dupdbbuilder
#
#    Dupdbbuilder is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Dupdpbuilder is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Dupdpbuilder.  If not, see <http://www.gnu.org/licenses/>.
#

function usage {
    echo "Usage: dupdbuilder [path] [storage-device-identifier]"
}

function checkargs {

    if [ $# -eq 0 ]; then
    echo "No arguments provided"
    usage
    exit 1
fi

if [ $# -ne 2 ]; then
    echo "You must provide exactly two arguments!"
    usage
    exit 1
fi

}


checkargs "$@"

# The user-supplied storage device identifier
DUP_DEV_ID=$2


find "${1}" -type f -print0 |  while IFS= read -r -d '' file; do

    # if it's a hard link, mark it as such
    if [ "$(stat -c %h -- "$file")" -gt 1 ]; then
	DUP_ISHARDLINK=1
    else
	DUP_ISHARDLINK=0
    fi

	# Path
	DUP_PATH=$(readlink -f "$file")

	# Size
	DUP_SIZE=$(stat -c "%s" "$file")

	# Name
	DUP_NAME=$(basename "$file")

	# MD5-Sum
	DUP_MD5=$(echo `md5sum "${file}" | awk '{ print $1 }'`)
	
	# Output SQL statement for this file
	echo -n "INSERT INTO DUPS (DEV_ID,MD5,SIZE,NAME,PATH,ISHARDLINK) "
	echo -n "VALUES ("
        echo -n "'"
		echo -n "$DUP_DEV_ID"
		echo -n "'"
	echo -n ","
		echo -n "'"
		echo -n "$DUP_MD5"
		echo -n "'"
	echo -n ","
		echo -n "$DUP_SIZE"
	echo -n ","
		echo -n "'"
		echo -n "$DUP_NAME"
		echo -n "'"
	echo -n ","
		echo -n "'"
		echo -n "$DUP_PATH"
		echo -n "'"
	echo -n ","
		echo -n "$DUP_ISHARDLINK"
	echo -n ");"

	# Newline
	echo 

done 

exit 0
